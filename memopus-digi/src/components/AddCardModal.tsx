import React, { useRef } from "react";
import '../css/AddCardModal.css';

interface AddCardModalProps {
    // je ne sais pas si c'est une bonne façon de faire
    onHide: () => void;
    onAdd: (columnId: number, questionValue: string, answerValue: string) => void; 
    columnId?: number;
}

const AddCardModal: React.FC<AddCardModalProps> = ({onHide, onAdd, columnId}) => {
    const newCardQuestion = useRef<HTMLInputElement>(null);
    const newCardAnswer = useRef<HTMLInputElement>(null);

    const addCard = async () => {
        if(newCardQuestion.current && newCardAnswer.current && columnId !== undefined){
            const questionValue = newCardQuestion.current.value;
            const answerValue = newCardAnswer.current.value;
            onAdd(columnId, questionValue, answerValue);
            onHide();
        }
    }
    const handleCloseModal = () => {
        onHide();
    };
    return (
        <>
            <div className="modal-background">
                <div className="add-card-modal">
                    <div className="modal-content">
                        <div className="d-flex justify-content-between modal-header">
                            <div className="modal-title h4">Gérer la card</div><button className="btn fs-4" onClick={handleCloseModal}>X</button>
                        </div>
                        <h3>Création de la Card</h3>
                        <div>
                            <p>Question : </p>
                            <input type="text" id="createQuestion" ref={newCardQuestion} />
                        </div>
                        <div>
                            <p>Réponse : </p>
                            <input type="text" id="createAnswer" ref={newCardAnswer} />
                        </div>
                        <div className="modalcreate">
                            <button className="btn btn-success" onClick={() => addCard()}>Envoyer</button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default AddCardModal;