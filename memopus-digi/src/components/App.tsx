// import React, { useEffect, useState } from 'react';
import '../css/App.css';
import Login from './Login';

function App() {

  return (
    <div className="App">
      <header className="App-header">
        <Login />
      </header>
    </div>
  );
}

export default App;
