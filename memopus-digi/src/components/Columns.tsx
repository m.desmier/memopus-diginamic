import React, { useEffect, useState } from "react";
import Data from "../services/Data";
import ColumnInterface from "./Interface/ColumnInterface";
import CardInterface from "./Interface/CardInterface";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faChevronLeft, faChevronRight, faGear } from '@fortawesome/free-solid-svg-icons';
import CardModal from "./CardModal";
import AddCardModal from "./AddCardModal";


const Columns: React.FC = () => {
    const [columns, setColumns] = useState<ColumnInterface[]>([]);
    const [cards, setCards] = useState<CardInterface[]>([]);
    const [position, setPosition] = useState(0); 
    const [showModal, setShowModal] = useState(false);
    const [selectedCard, setSelectedCard] = useState<CardInterface | null>(null);
    const [showAddModal, setShowAddModal] = useState(false);
    const [selectedColumnId, setSelectedColumnId] = useState<number | null>(null);

    // récupère les données des colonnes
    useEffect(() => {
        async function fetchColumns() {
            try {
                const columnsData = await Data.getColumns();
                setColumns(columnsData);
            } catch (error) {
                console.error('Erreur lors de la récupération des colonnes :', error);
            }
        }
        fetchColumns();
    }, []);
    // récupère les données des cards
    useEffect(() => {
        async function fetchCards() {
            try {
                const cardsData = await Data.getCards();
                setCards(cardsData);
            } catch (error) {
                console.error('Erreur lors de la récupération des cartes :', error);
            }
        }
        fetchCards();
    }, []);
    // déplacement des cards
    const handleMoveLeft = async (event: React.MouseEvent<HTMLButtonElement>, cardId: number) => {
        if(position > 0){
            const newColumnId = columns[position - 1].id;
            Data.updateCardPosition(cardId, newColumnId);
            setPosition(position - 1); // déplace à gauche
            setColumns(updatedColumns => {
                return updatedColumns.map(column => {
                    const updatedCards = column.cards.map(card => {
                        if(card.id === cardId){
                            return {...card, column: newColumnId};
                        }
                        return card;
                    });
                    return {...column, cards: updatedCards};
                });
            });
        }
    };
    const handleMoveRight = async (event: React.MouseEvent<HTMLButtonElement>, cardId: number) => {
        if(position < columns.length -1){
            const newColumnId = columns[position + 1].id;
            Data.updateCardPosition(cardId, newColumnId);
            setPosition(position + 1);
            setColumns(updatedColumns => {
                return updatedColumns.map(column => {
                    const updatedCards = column.cards.map(card => {
                        if (card.id === cardId) {
                            return { ...card, column: newColumnId };
                        }
                        return card;
                    });
                    return { ...column, cards: updatedCards };
                });
            });
        }
    };
    // fin déplacement des cards
    const handleUpdateCard = (updatedCard: CardInterface) => {
        const updatedCards = cards.map(card => {
            if (card.id === updatedCard.id) {
                return updatedCard;
            }
            return card;
        });

        setCards(updatedCards);
        closeModal();
    };
    // gestion de la modal modificatiojn et suppression
    const openModal = (card: CardInterface) => {
        setSelectedCard(card);
        setShowModal(true);
    };
    const closeModal = () => {
        setSelectedCard(null);
        setShowModal(false);
    };
    // gestion de la modal pour ajout
    const openAddModal = (columnId: number) => {
        setSelectedColumnId(columnId);
        setShowAddModal(true);
    };
    const closeAddModal = () => {
        setShowAddModal(false);
    };
    const handleDeleteCard = async (cardId: number) => {
        await Data.deleteCard(cardId);
        const updateCard = cards.filter(card => card.id !== cardId);
        setCards(updateCard);
        closeModal();
    };
    const handleAddCard = async (columnId: number, questionValue: string, answerValue: string) => {
        const newCard: CardInterface = {
            id: cards.length + 1,
            uid: cards.length + 1,
            question: questionValue,
            answer: answerValue,
            column: columnId,
            selected: false,
            tid:cards.length + 1,
        }
        await Data.addCard(newCard); 
        setCards([...cards, newCard]);
        closeAddModal();
    };
    return (
        <>
            <div className="containerColumns">
                {columns.map((column) => (
                    <div key={column.id} className="labelColumns me-3">
                        <button className="btn btn-success btnColumns" onClick={() => openAddModal(column.id)}>+</button>
                        <h3>{column.label}</h3>
                        {/* ajouter les termes */}
                        <div className="cards">
                            {cards
                                .filter((card) => card.column === column.id)
                                .map((card) => {
                                    return (
                                        <div key={card.id} className="card">
                                            <div className="card-body">
                                                <h5 className="card-title"><button className="iconChevrons" onClick={(event) => handleMoveLeft(event, card.id)}><FontAwesomeIcon icon={faChevronLeft} /></button> {card.question} <button className="iconChevrons" onClick={(event) => handleMoveRight(event, card.id)}><FontAwesomeIcon icon={faChevronRight} /></button></h5>
                                                <div className="d-flex align-items-center">
                                                    <button className="btn btn-warning mt-4">Proposer une réponse</button>
                                                    <button className="btn btn-link ms-3 fs-3 mt-4 param" onClick={() => openModal(card)}>
                                                        <FontAwesomeIcon icon={faGear} />
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })
                            }
                        </div>
                    </div>
                ))}
            </div>
            {showModal && selectedColumnId !== null && (
                <CardModal card={selectedCard} onHide={closeModal} onUpdate={handleUpdateCard} onDelete={handleDeleteCard} />
            )}
            {showAddModal && selectedColumnId !== null && (
                <AddCardModal onHide={closeAddModal} onAdd={handleAddCard} columnId={selectedColumnId} />
            )}
        </>
    );
}

export default Columns;