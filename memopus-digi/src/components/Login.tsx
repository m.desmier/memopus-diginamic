import React, { useState } from "react";
import Data from "../services/Data";
import LoginInterface from "./Interface/LoginInterface";
import { useNavigate } from 'react-router-dom';
import logoPoulpi from '../css/poulpi.png';
import '../css/Login.css';

const Login: React.FC = () => {
    const navigate = useNavigate();
    // useState est un hook qui permet de gérer l'état local dans un composant fonctionnel
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    // gestion de message d'erreur
    const [errorMessage, setErrorMessage] = useState('');

    const handleLogin = async () => {
        // appel de la fonction onLogin avec les valeurs de username et password
        try{
            const data: LoginInterface[] = await Data.getUsers();
            const user = data.find((user: any) => user.username === username && user.password === password);

            if (user) {
                console.log('Connexion réussie !', user);
                navigate('/dashboard');
                // navigate('/home'); 
                //je n'ai pas pu finir a temps donc toute la logique pour les term est pas faite
            } else {
                console.log('Nom d\'utilisateur ou mot de passe incorrect.');
                setErrorMessage('Nom d\'utilisateur ou mot de passe incorrect !');
            }
        }catch(error){
            console.error('Erreur de connexion :', error);
            setErrorMessage('Une erreur s\'est produite lors de la connexion !');
        }
    }
    return (
        <>
            <h1 className="titleMemo"> <img src={logoPoulpi} alt="logo poulpi" /> MemoDigi</h1>
            <div className="container-connexion">
                <h2 className="titleLogin">Connexion</h2>
                {errorMessage && <div className="error-message">{errorMessage}</div>}
                    <div className="mb-3">
                        <label className="form-label">Nom d'utilisateur :</label>
                        <input type="text" className="form-control" value={username} onChange={(e) => setUsername(e.target.value)} />
                    </div>
                    <div className="mb-3">
                        <label className="form-label">Mot de passe :</label>
                        <input type="password" className="form-control" value={password} onChange={(e) => setPassword(e.target.value)} />
                    </div>
                    <button onClick={handleLogin} className="btn btn-warning">Se connecter</button>
            </div>
        </>
    );
}

export default Login;