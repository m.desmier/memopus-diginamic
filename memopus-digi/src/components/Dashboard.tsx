import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGear, faArrowRightFromBracket } from '@fortawesome/free-solid-svg-icons';
import '../css/Dashboard.css';
import logoPoulpi from '../css/poulpi.png';
import Columns from './Columns';
import {Link} from 'react-router-dom';

const Dashboard = () => {
    return (
        <>
        <header className="Dashboard-header">
            <div className='container'>
                <h1> <img src={logoPoulpi} alt="logo poulpi" /> MemoDigi</h1>
                <div className='admin'>
                    <FontAwesomeIcon icon={faGear} /> Activer le mode admin
                </div>
                <div className='deco'>
                    <FontAwesomeIcon icon={faArrowRightFromBracket} /> Déconnexion
                </div>
            </div>
        </header>
        <main className='Dashboard-main'>
                <section id="bouton-terms" className="d-flex justify-content-center">
                    <button id="add-terms" className="btn btn-success m-2" title="Ajouter une thématique">+</button>
                    <Link to="/home">
                        <button className="btn btn-secondary m-2">Page terms</button>
                    </Link>
                </section>
            <Columns />
        </main>
        </>
    );
}

export default Dashboard;