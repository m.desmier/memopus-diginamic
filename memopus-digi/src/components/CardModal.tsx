import React, { useState, useEffect, useRef } from "react";
import CardInterface from "./Interface/CardInterface";
import '../css/CardModal.css';

interface CardModalProps {
    card: CardInterface | null;
    onHide: () => void;
    onUpdate: (updatedCard: CardInterface) => void;
    onDelete: (cardId: number) => void;
}

const CardModal: React.FC<CardModalProps> = ({ card, onHide, onUpdate, onDelete }) => {
    const [modalVisible, setModalVisible] = useState(true);
    const updateQuestion = useRef<HTMLInputElement>(null);
    const updateAnswer = useRef<HTMLInputElement>(null);

    useEffect(() => {
        // Met à jour l'états des valeurs
        if(card) {
            if (updateQuestion.current) {
                updateQuestion.current.value = card.question;
            }
            if (updateAnswer.current) {
                updateAnswer.current.value = card.answer;
            }
        }
    }, [card]);

    const handleCloseModal = () => {
        setModalVisible(false); // Fermer la modal
        onHide();
    };

    const handleUpdateCard = () => {
        console.log("card:", card);
        if(card && updateQuestion.current && updateAnswer.current){
            const updatedCard: CardInterface = {
                ...card,
                question: updateQuestion.current.value,
                answer: updateAnswer.current.value,
            };
            console.log(`updatedCard `, updatedCard);
            updateQuestion.current.value = updatedCard.question;
            updateAnswer.current.value = updatedCard.answer;
            onUpdate(updatedCard);
            onHide();
        }
    };

    const handleDeleteCard = () => {
        if(card){
            onDelete(card.id);
        }
    };

    return (
        <>
            <div className="modal-background">
                <div className="card-modal">
                    {card && (
                        <>
                            <div className="modal-content">
                                <div className="d-flex justify-content-between modal-header">
                                    <div className="modal-title h4">Gérer la card</div><button className="btn fs-4" onClick={handleCloseModal}>X</button>
                                </div>
                                <h3>Modification de la Card</h3>
                                <div>
                                    <label htmlFor="updatedQuestion">Question : </label>
                                    <input type="text" id="updatedQuestion" defaultValue={card.question} ref={updateQuestion} />
                                </div>
                                <div>
                                    <label htmlFor="updatedAnswer">Réponse : </label>
                                    <input type="text" id="updatedAnswer" defaultValue={card.answer} ref={updateAnswer} />
                                </div>
                                <div className="modalModif">
                                    <button className="btn btn-success" onClick={handleUpdateCard}>Envoyer</button>
                                </div>
                                <div className="modalSuppr">
                                    <button className="btn btn-danger" onClick={handleDeleteCard}>Supprimer la carte</button>
                                </div>
                            </div>
                        </>
                    )}
                </div>
            </div>
        </>
    );
};

export default CardModal;