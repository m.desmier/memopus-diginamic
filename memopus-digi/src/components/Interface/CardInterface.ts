export default interface CardInterface {
    id: number;
    uid: number;
    question: string;
    answer: string;
    column: number;
    selected: boolean;
    tid: number;
}