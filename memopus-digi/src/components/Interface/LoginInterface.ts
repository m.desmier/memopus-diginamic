export default interface LoginInterface {
    id: number;
    username: string; 
    password: string;
}