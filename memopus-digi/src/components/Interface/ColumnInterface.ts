export default interface ColumnInterface {
    id: number;
    label: string;
    cards: any[];
    terms: any[];
}