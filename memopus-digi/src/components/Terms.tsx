import React from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGear, faArrowRightFromBracket } from '@fortawesome/free-solid-svg-icons';
import logoPoulpi from '../css/poulpi.png';
import '../css/terms.css';

const Terms: React.FC = () => {
    return (
        <>
            <header>
                <div className='container'>
                    <h1> <img src={logoPoulpi} alt="logo poulpi" /> MemoDigi</h1>
                    <div className='admin'>
                        <FontAwesomeIcon icon={faGear} /> Activer le mode admin
                    </div>
                    <div className='deco'>
                        <FontAwesomeIcon icon={faArrowRightFromBracket} /> Déconnexion
                    </div>
                </div>
            </header>
            <main className="main-container">
                <section id="bouton-terms" className="d-flex justify-content-center">
                    <button id="add-terms" className="btn btn-success m-2" title="Ajouter une thématique">+</button>
                    <button className="btn btn-secondary m-2">Catégorie</button>
                </section>
                <div className="row d-flex justify-content-center" id="tableau">
                    <table className="mt-5 table table-dark table-hover table-striped text-center">
                        <thead>
                            <tr>
                                <th>Thématique</th>
                                <th>À apprendre</th>
                                <th>Je sais un peu</th>
                                <th>Je sais bien</th>
                                <th>Je sais parfaitement</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table> 
                </div>
            </main>
            <footer>
                <ul className="list-unstyled d-flex justify-content-center mt-5">
                    <li className="m-2">Accueil</li>
                    <li className="m-2">Mes tableaux</li>
                    <li className="m-2">Les tableaux des autres</li>
                    <li className="m-2">A propos de l'application</li>
                </ul>
            </footer>
        </>
    );
}

export default Terms;
