import CardInterface from "../components/Interface/CardInterface";

export default class Data {
    static url: string = "http://localhost:3001/";
    static urlCard: string = "http://localhost:3001/cards";

    static async getUsers(){
        const response = await fetch(`${this.url}users`);
        const data = await response.json();
        return data;
    }
    
    static async getColumns() {
        const response = await fetch(`${this.url}columns`);
        const columns = await response.json();
        return columns;
    }

    static async getCards() {
        const response = await fetch(`${this.url}cards`);
        const cards = await response.json();
        return cards;
    }

    static async getTerms(){
        const response = await fetch(`${this.url}terms`);
        const terms = await response.json();
        return terms;
    }

    // déplacement des boutons
    static updateCardPosition(cardId: number, newColumnId: number) {
        return fetch(this.urlCard+"/"+cardId, 
        {
            method: 'PATCH',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ column: newColumnId }),
        })
        .then((res) => {return res.json() })
        .catch((error) => {console.log(error) })
    }

    // modification d'une card
    static updateCard(card: CardInterface): Promise<CardInterface> {
        const {id, question, answer, column} = card;
        const updatedCard = {question, answer, column};
        return fetch(this.urlCard+"/"+id,
        {
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            method: 'PUT',
            body: JSON.stringify(updatedCard),
        })
        .then((res) => {return res.json() })
        .catch((error) => {console.log(error) })
    }

    // suppression d'une card
    static deleteCard(id: number) {
        return fetch(this.urlCard+"/"+id,
        {
            method: 'DELETE',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        })
        .then((res) => {return res.json() })
        .catch((error) => {console.log(error) })
    }
    static addCard(card: CardInterface): Promise<CardInterface> {
        return fetch(this.urlCard,{
            headers: {
                'Accept': 'application/json',
                'Content-type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(card),
        })
        .then((res) => {return res.json() })
        .catch((error) => {console.log(error) })
    }
}